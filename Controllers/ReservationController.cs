using LoginApi.Handler.Interfaces;
using LoginApi.Model;
using Microsoft.AspNetCore.Mvc;

namespace LoginApi.Controllers;

[ApiController]
public class ReservationController : ControllerBase
{
    private readonly IReservationHandler _reservationHandler;
    private readonly ILoginHandler _loginHandler;

    public ReservationController(IReservationHandler reservationHandler, ILoginHandler loginHandler)
    {
        _reservationHandler = reservationHandler;
        _loginHandler = loginHandler;
    }

    [Route("api/Reservation")]
    [HttpPost]
    public async Task<ActionResult<ReservationItem>> Reservation(string firstName, string secondName,
        int telephoneNumber, string email,
        string time, string date, string barberName)
    {
        var getCustomerId = await _loginHandler.GetCustomerId(barberName);

        if (getCustomerId == 0)
        {
            return StatusCode(400, new
            {
                message = "Es konnte kein Mitarbeiter mit dem Namen gefunden werden"
            });
        }


        var validation = await _reservationHandler.ValidateReservation(getCustomerId, time, date);

        if (!validation)
        {
            return StatusCode(403, new
            {
                message = "Der Termin ist leider schon ausgebucht!"
            });
        }


        var reservation =
            await _reservationHandler.Reservation(firstName, secondName, telephoneNumber, email, date, time,
                getCustomerId);


        return StatusCode(200, new
        {
            message = "Termin erfolgreich reserviert! " + barberName + " wird sich um " + time + " um dich kümmern",
            personalData = new
            {
                firstName = firstName,
                secondName = secondName,
                telephoneNumber = telephoneNumber,
                email = email,
                date = date,
                time = time,
                barber = barberName,
            }
        });
    }

    [Route("api/CancelReservation")]
    [HttpPost]
    public async Task<ActionResult> CancelReservation(string firstName, string secondName)
    {
        var cancel = await _reservationHandler.CancelReservation(firstName, secondName);

        if (!cancel)
        {
            return StatusCode(400, new
            {
                message = "Reservierung konnte nicht abgesagt werden!"
            });
        }

        return StatusCode(200, new
        {
            message = "Reservierung wurde erfolgreich abgesagt!"
        });
    }


    [Route("api/GetReservation")]
    [HttpGet]
    public async Task<ActionResult<List<ReservationItem>>> GetAllReservation()
    {
        var reservations = await _reservationHandler.GetAllReservations();

        var reservationList = new List<ReservationItem>();

        foreach (var item in reservations)
        {
            var barberName = await _loginHandler.GetBarberById(item.customer_id);

            var reservationItem = new ReservationItem
            {
                first_name = item.first_name,
                reservation_time = item.reservation_time,
                barber = barberName,
                is_finished = item.is_finished,
                reservation_id = item.reservation_id,
            };

            reservationList.Add(reservationItem);
        }

        int count = reservationList.Count(item => item.is_finished == false); 
        
        if (count == 0 )
        {
            return StatusCode(200, new { message = "Keine offenen Reservierungen gefunden!" }); 
        }

        return StatusCode(200, new {customerData = reservationList, message =""});
    }

    [Route("api/UpdateReservationStatus")]
    [HttpPatch]
    public async Task<ActionResult<ReservationItem>> UpdateReservationStatus(int reservationId)
    {
        var reservationStatus = await _reservationHandler.UpdateReservationStatus(reservationId);

        var finishedReservation = await _reservationHandler.GetReservationById(reservationId);

        if (finishedReservation == null)
        {
            return StatusCode(400, new { message = "Fehler beim beenden des Termins" });
        }

        var reservationItem = new ReservationItem
        {
            is_finished = finishedReservation.is_finished,
            reservation_id = reservationId,
            first_name = finishedReservation.first_name,
        };

        return StatusCode(200, reservationItem);
    }

    [Route("api/DashboardReservationData")]
    [HttpGet]
    public async Task<ActionResult<DashboardItem>> GetAllDashboardReservationData()
    {
        var allReservation = await _reservationHandler.GetAllDashboardReservationData();

        var allFinishedReservations = await _reservationHandler.GetFinishedReservations();


        var dailyReservations = new DashboardItem
        {
            DailyReservations = allReservation,
            FinishedReservations = allFinishedReservations,
            OpenReservations = allReservation - allFinishedReservations,
        };
        
        return dailyReservations;
    }

    [Route("api/BarberDashboardReservationData")]
    [HttpGet]
    public async Task<ActionResult<DashboardItem>> GetBarberDashboardReservationData(int barberId)
    {
        var barberDashboardData = await _reservationHandler.GetBarberDashboardAllReservation(barberId);

        var finishedBarberDashboardData = await _reservationHandler.GetBarberDashbaordFinishedReservation(barberId);


        var dailyReservations = new DashboardItem
        {
            DailyReservations = barberDashboardData,
            FinishedReservations = finishedBarberDashboardData,
            OpenReservations = barberDashboardData - finishedBarberDashboardData
        };

        return dailyReservations; 

    }
    
    [Route("api/GetReservationByBarberId")]
    [HttpGet]
    public async Task<ActionResult<List<ReservationItem>>> GetReservationByBarberId(int barberId)
    {

        var reservationList = new List<ReservationItem>(); 
        
        var barberReservations = await _reservationHandler.GetReservationByBarberId(barberId);
        
        var openReservations = await _reservationHandler.GetBarberDashboardOpenReservation(barberId);

        
        foreach (var item in barberReservations)
        {
            var barberName = await _loginHandler.GetBarberById(item.customer_id);

            var reservationItem = new ReservationItem
            {
                first_name = item.first_name,
                reservation_time = item.reservation_time,
                barber = barberName,
                is_finished = item.is_finished,
                reservation_id = item.reservation_id,
            };

            reservationList.Add(reservationItem);
        }
        
        if(openReservations == 0)
        {
            return StatusCode(200, new {customerData = reservationList, message = "Keine offenen Reservierungen gefunden!"} );
        }

        return StatusCode(200, new {customerData = reservationList, message =""});
    }
}