using LoginApi.Handler.Interfaces;
using LoginApi.Model;
using Microsoft.AspNetCore.Mvc;

namespace LoginApi.Controllers;

[ApiController]
public class LoginController : ControllerBase
{
    private readonly ILoginHandler _loginHandler;

    public LoginController(ILoginHandler loginHandler)
    {
        _loginHandler = loginHandler;
    }

    [Route("api/Registration")]
    [HttpPost]
    public async Task<ActionResult<LoginItem>> Registration(string username, string password)
    {
        var registrationValidation = await _loginHandler.RegistrationValidation(username);

        if (registrationValidation)
        {
            await _loginHandler.Registration(username, password);


            return StatusCode(200, new
            {
                username = username,
                password = password,
                message = "Registrierung erfolgreich!"
            });
        }

        return StatusCode(400, new
        {
            username = username,
            password = password,
            message = "Dieser Nutzername ist bereits vergeben, bitte wählen Sie einen anderen"
        });
    }

    [Route("api/Login")]
    [HttpPost]
    public async Task<ActionResult> Login(string username, string password)
    {
        int parsedUsername;
        bool isUsernameValid = int.TryParse(username, out parsedUsername);


        int parsedPassword;
        bool isPasswordValid = int.TryParse(password, out parsedPassword);


        if (isUsernameValid && isPasswordValid)
        {
            var personalLogin =
                await _loginHandler.PersonalLogin(Convert.ToInt32(parsedUsername), Convert.ToInt32(parsedPassword));

            if (personalLogin)
            {
                return StatusCode(200,
                    new
                    {
                        username = username, password = password, admin = personalLogin,
                        message =
                            "Herzlich Willkommen zurück, wirf doch mal einen Blick in die Reservierung, vielleicht ist ja was für dich dabei!"
                    });
            }
        }

        var login = await _loginHandler.LoginValidation(username, password);

        if (!login)
        {
            return StatusCode(400, new
            {
                username = username,
                password = password,
                message = "Der Nutzername oder das Passwort ist falsch! Bitte versuchen Sie es erneut",
                admin = false
            });
        }

        return StatusCode(200, new
        {
            username = username,
            password = password,
            admin = false,
            message = "Anmeldung Erfolgreich!"
        });
    }

    [Route("api/GetBarbers")]
    [HttpGet]
    public async Task<ActionResult<List<Barber>>> GetBarber()
    {
        var barberData = await _loginHandler.GetBarber();

        return barberData;
    }
}