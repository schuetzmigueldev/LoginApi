using System.Runtime.InteropServices.JavaScript;
using Dapper;
using LoginApi.Helper;
using LoginApi.Model;
using AutoMapper;
namespace LoginApi.Repositorys;

public class LoginRepository : ILoginRepository
{
    private readonly ISqlConnectionProvider _sqlConnectionProvider;
    private readonly IMapper _mapper;

    public LoginRepository(ISqlConnectionProvider connectionProvider, IMapper mapper)
    {
        _sqlConnectionProvider = connectionProvider;
        _mapper = mapper;
    }

    public async Task<LoginItem> Registration(string username, string password)
    {
        const string postRegistrationQuery = @"INSERT INTO login_table(username,password) 
                                        VALUES (@usernameContent, @passwordContent )";

        using var connection = await _sqlConnectionProvider.GetConnection();
        var postLogin = await connection.QueryFirstOrDefaultAsync<LoginItem>(postRegistrationQuery,
            new { usernameContent = username, passwordContent = password });
        ;

        return postLogin;
    }

    public async Task<bool> RegistrationValidation(string username)
    {
        const string registrationValidationQuery =
            @"SELECT COUNT(*) AS count 
                                FROM login_table
                                WHERE username = @userInput";


        using var connection = await _sqlConnectionProvider.GetConnection();

        var count = await connection.QueryFirstOrDefaultAsync<int>(registrationValidationQuery,
            new { userInput = username });

        // if user exist, return false, else return true; 
        if (count < 1)
        {
            return true;
        }

        return false;
    }


    public async Task<bool> LoginValidation(string username, string password)
    {
        const string postLoginQuery = @"SELECT COUNT(*) AS count 
                                FROM login_table
                                WHERE username = @userInput AND password = @passwordInput";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var count = await connection.QueryFirstOrDefaultAsync<int>(postLoginQuery,
            new { userInput = username, passwordInput = password });

        if (count < 1)
        {
            return false;
        }

        return true;
    }

    public async Task<bool> PersonalLogin(int username, int password)
    {
        const string query =
            @"SELECT COUNT(*) As count FROM personal_login WHERE username = @userInput AND password = @passwordInput";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var count = await connection.QueryFirstOrDefaultAsync<int>(query,
            new { userInput = username, passwordInput = password });

        if (count < 1)
        {
            return false; 
        }

        return true; 

    }
    
    public async Task<List<Barber>> GetBarber()
    {
        const string getBarber = @"SELECT * FROM barber_table";

        using var connection = await _sqlConnectionProvider.GetConnection();


        var barberData = await connection.QueryAsync<Barber>(getBarber);

        return _mapper.Map<List<Barber>>(barberData);
    }

    public async Task<int> GetCustomerId(string name)
    {
        const string getCustomerIdString =
            @"SELECT customer_id FROM barber_table WHERE first_name = @firstNameContent ";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var customerId =
            await connection.QueryFirstOrDefaultAsync<int>(getCustomerIdString, new { firstNameContent = name });

        return customerId;
    }

    public async Task<string> GetBarberById(int employeeId)
    {
        const string getBarberByIdString = @"SELECT first_name FROM barber_table WHERE customer_id = @employeeIdContent";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var barberName = await connection.QueryFirstAsync<string>(getBarberByIdString, new { employeeIdContent = employeeId });


        return barberName; 
    }
    
}