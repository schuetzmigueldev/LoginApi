using Dapper;
using LoginApi.Helper;
using LoginApi.Model;
using AutoMapper;

namespace LoginApi.Repositorys;

public class ReservationRepository : IReservationRepository
{
    private readonly ISqlConnectionProvider _sqlConnectionProvider;
    private readonly IMapper _mapper;

    public ReservationRepository(ISqlConnectionProvider sqlConnectionProvider, IMapper mapper)
    {
        _sqlConnectionProvider = sqlConnectionProvider;
        _mapper = mapper;
    }

    public async Task<bool> ValidateReservation(int customerId, string time, string date)
    {
        const string validateReservationString =
            @"SELECT COUNT(*) AS count FROM reservation_table WHERE customer_id = @customerIdContent AND reservation_time = @timeContent AND reservation_date = @dateContent";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var count = await connection.QueryFirstOrDefaultAsync<int>(validateReservationString,
            new { customerIdContent = customerId, timeContent = time, dateContent = date });

        if (count < 1)
        {
            return true;
        }

        return false;
    }

    public async Task<ReservationItem> Reservation(string firstName, string secondName, int telephoneNumber,
        string email,
        string date, string time, int customerId)
    {
        const string reservationQuery =
            @"INSERT INTO reservation_table(first_Name,second_name,telephone_number,email,reservation_date,reservation_time,customer_id) 
                                          VALUES (@firstNameContent,@secondNameContent,@telephoneNumberContent,@emailContent,@dateContent,@timeContent,@customerIdContent)";

        using var connection = await _sqlConnectionProvider.GetConnection();
        var reservation = await connection.QueryFirstOrDefaultAsync<ReservationItem>(reservationQuery,
            new
            {
                firstNameContent = firstName, secondNameContent = secondName, telephoneNumberContent = telephoneNumber,
                emailContent = email, dateContent = date, timeContent = time, customerIdContent = customerId,
            });

        return reservation;
    }

    public async Task<bool> CancelReservation(string firstName, string secondName)
    {
        const string cancelReservationQuery =
            @"DELETE FROM reservation_table WHERE first_name = @firstNameContent AND second_name = @secondNameContent";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var cancelReservation = await connection.ExecuteAsync(cancelReservationQuery,
            new { firstNameContent = firstName, secondNameContent = secondName });

        if (cancelReservation != 1)
        {
            return false;
        }

        return true;
    }


    public async Task<List<ReservationItem>> GetAllReservations()
    {
        const string getAllReservationString = @"SELECT * FROM reservation_table";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var reservationData = await connection.QueryAsync<ReservationItem>(getAllReservationString);

        
        return _mapper.Map<List<ReservationItem>>(reservationData);
    }

    public async Task<ReservationItem> GetReservationById(int reservationId)
    {
        const string getReservationByIdString =
            @"SELECT * FROM reservation_table WHERE reservation_id = @reservationIdContent";

        using var connecton = await _sqlConnectionProvider.GetConnection();

        var reservation = await connecton.QueryFirstOrDefaultAsync<ReservationItem>(getReservationByIdString,
            new { reservationIdContent = reservationId });

        return reservation;
    }

    public async Task<bool> UpdateReservationStatus(int reservationId)
    {
        const string updateReservationStatusString =
            @"UPDATE reservation_table SET is_finished = true WHERE reservation_id = @reservationContent ";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var reservationStatus = await connection.QueryFirstOrDefaultAsync<bool>(
            updateReservationStatusString,
            new { reservationContent = reservationId });


        return reservationStatus;
    }

    public async Task<int> GetAllDashboardReservationData()
    {
        const string getAllDashboardData = @"SELECT COUNT(*) AS count FROM reservation_table";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var allReservationCount = await connection.QueryFirstOrDefaultAsync<int>(getAllDashboardData);

        return allReservationCount;
    }

    public async Task<int> GetBarberDashboardReservationData(int barberId)
    {
        const string query = @"SELECT COUNT(*) As count FROM reservation_table WHERE customer_id = @barberIdContent"; 

        using var connection = await _sqlConnectionProvider.GetConnection();

        var barberReservationCount =
            await connection.QueryFirstOrDefaultAsync<int>(query, new { barberIdContent = barberId });


        return barberReservationCount;
    }
    
    public async Task<int> GetBarberDashbaordFinishedReservation(int barberId)
    {
        const string query = @"SELECT COUNT(*) As count FROM reservation_table WHERE customer_id = @barberIdContent AND is_finished = true"; 

        using var connection = await _sqlConnectionProvider.GetConnection();

        var barberReservationCount =
            await connection.QueryFirstOrDefaultAsync<int>(query, new { barberIdContent = barberId });


        return barberReservationCount;
    }

    public async Task<int> GetBarberDashboardOpenReservation(int barberId)
    {
        const string query = @"SELECT COUNT(*) As count FROM reservation_table WHERE customer_id = @barberIdContent AND is_finished = false";


        using var connection = await _sqlConnectionProvider.GetConnection();

        var openReservations =
            await connection.QueryFirstOrDefaultAsync<int>(query, new { barberIdContent = barberId });

        return openReservations; 

    }


    public async Task<int> GetFinishedReservations()
    {
        const string getFinishedDashboardData =
            @"SELECT COUNT(*) AS count FROM reservation_table WHERE is_finished = true";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var getAllFinishedReservation = await connection.QueryFirstOrDefaultAsync<int>(getFinishedDashboardData);

        return getAllFinishedReservation;
    }

    public async Task<List<ReservationItem>> GetReservationByBarberId(int barberId)
    {
        const string getBarberReservationString = @"SELECT * FROM reservation_table WHERE customer_id = @employeId ";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var getBarberReservations =
            await connection.QueryAsync<ReservationItem>(getBarberReservationString, new { employeId = barberId });

        return _mapper.Map<List<ReservationItem>>(getBarberReservations);
    }
}