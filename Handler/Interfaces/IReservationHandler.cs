using LoginApi.Model;

namespace LoginApi.Handler.Interfaces;

public interface IReservationHandler
{
    public Task<List<ReservationItem>> GetAllReservations();

    public Task<bool> ValidateReservation(int customerId, string time, string date);

    public Task<ReservationItem> Reservation(string firstName, string secondName, int telephoneNumber, string email,
        string date, string time, int customerId);

    public Task<bool> CancelReservation(string fistName, string secondName);

    public Task<ReservationItem> GetReservationById(int reservationId);
    public Task<bool> UpdateReservationStatus(int reservationId);

    public Task<int> GetAllDashboardReservationData();
    public Task<int> GetBarberDashboardAllReservation(int barberId);
    public Task<int> GetBarberDashbaordFinishedReservation(int barberId); 
    public Task<List<ReservationItem>> GetReservationByBarberId(int barberId);

    public Task<int> GetBarberDashboardOpenReservation(int barberId); 
    

    public Task<int> GetFinishedReservations();
}