using LoginApi.Model;

namespace LoginApi.Handler.Interfaces;

public interface ILoginHandler
{
    public Task<LoginItem> Registration(string username, string password);

    public Task<bool> RegistrationValidation(string username);

    public Task<bool> LoginValidation(string username, string password);

    public Task<bool> PersonalLogin(int username, int password); 
    public Task<List<Barber>> GetBarber();

    public Task<int> GetCustomerId(string name);
    public Task<string> GetBarberById(int employeeId); 
    

}