using AutoMapper.Internal.Mappers;
using LoginApi.Handler.Interfaces;
using LoginApi.Model;
using LoginApi.Repositorys;

namespace LoginApi.Handler;

public class LoginHandler : ILoginHandler
{
    private readonly ILoginRepository _loginRepository;

    public LoginHandler(ILoginRepository loginRepository)
    {
        _loginRepository = loginRepository;
    }

    public async Task<LoginItem> Registration(string username, string password)
    {
        return await _loginRepository.Registration(username, password);
    }

    public async Task<bool> RegistrationValidation(string username)
    {
        return await _loginRepository.RegistrationValidation(username);
    }


    public async Task<bool> LoginValidation(string username, string password)
    {
        return await _loginRepository.LoginValidation(username, password);
    }

    public async Task<bool> PersonalLogin(int username, int password)
    {
        return await _loginRepository.PersonalLogin(username, password);
    }

    public async Task<List<Barber>> GetBarber()
    {
        return await _loginRepository.GetBarber();
    }

    public async Task<int> GetCustomerId(string name)
    {
        return await _loginRepository.GetCustomerId(name);
    }

    public async Task<string> GetBarberById(int employeeId)
    {
        return await _loginRepository.GetBarberById(employeeId);
    }
}