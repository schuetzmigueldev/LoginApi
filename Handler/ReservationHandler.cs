using LoginApi.Handler.Interfaces;
using LoginApi.Model;
using LoginApi.Repositorys;

namespace LoginApi.Handler;

public class ReservationHandler: IReservationHandler
{

    private readonly IReservationRepository _reservationRepository;

    public ReservationHandler(IReservationRepository reservationRepository)
    {
        _reservationRepository = reservationRepository; 
    }
    
    
    public async Task<bool> ValidateReservation(int customerId, string time, string date)
    {
        return await _reservationRepository.ValidateReservation(customerId, time, date);
    }

    public async Task<ReservationItem> Reservation(string firstName, string secondName, int telephoneNumber,
        string email,
        string date, string time, int customerId)
    {
        return await _reservationRepository.Reservation(firstName, secondName, telephoneNumber, email, date, time,
            customerId);
    }

    public async Task<bool> CancelReservation(string firstName, string secondName)
    {
        return await _reservationRepository.CancelReservation(firstName, secondName);
    }
    
    public async Task<List<ReservationItem>> GetAllReservations()
    {
        return await _reservationRepository.GetAllReservations();
    }

    public async Task<ReservationItem> GetReservationById(int reservationId)
    {
        return await _reservationRepository.GetReservationById(reservationId); 
    }

    public async Task<bool> UpdateReservationStatus(int reservationId)
    {
        return await _reservationRepository.UpdateReservationStatus(reservationId); 
    }

    public async Task<int> GetAllDashboardReservationData()
    {
        return await _reservationRepository.GetAllDashboardReservationData(); 
    }

    public async Task<int> GetBarberDashboardAllReservation(int barberId)
    {
        return await _reservationRepository.GetBarberDashboardReservationData(barberId); 
    }

    public async Task<int> GetBarberDashbaordFinishedReservation(int barberId)
    {
        return await _reservationRepository.GetBarberDashbaordFinishedReservation(barberId); 
    }
     
    public async Task<int> GetFinishedReservations()
    {
        return await _reservationRepository.GetFinishedReservations(); 
    }

    public async Task<int> GetBarberDashboardOpenReservation(int barberId)
    {
        return await _reservationRepository.GetBarberDashboardOpenReservation(barberId); 
    }

    public async Task<List<ReservationItem>> GetReservationByBarberId(int barberId)
    {
        return await _reservationRepository.GetReservationByBarberId(barberId); 
    }
    
    

    
}