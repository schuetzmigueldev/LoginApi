namespace LoginApi.Model;

public class Barber
{
    public int id { get; set; }
    public string first_name { get; set; }
    public string second_name { get; set; }
    public string position { get; set; }
    public int customer_id { get; set; }
}