using System.Net.Mime;

namespace LoginApi.Model;

public class LoginItem
{
    public int id { get; set; }
    public string username { get; set; }
    public string password { get; set; }
}