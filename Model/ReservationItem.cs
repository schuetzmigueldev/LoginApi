namespace LoginApi.Model;

public class ReservationItem
{
    public int id { get; set; }
    public string? first_name { get; set; }
    public string? second_name { get; set; }
    public int? telephone_number { get; set; }
    public string? email { get; set; }
    public string? reservation_date { get; set; }
    public string? reservation_time { get; set; }
    public int customer_id { get; set; }
    public string barber { get; set; }
    public int? reservation_id { get; set; }

    public bool? is_finished { get; set; }
    
    public string? message { get; set; }
}

public class DashboardItem
{
    public int DailyReservations { get; set;}
    public int FinishedReservations { get; set; }
    public int OpenReservations { get; set; }
}