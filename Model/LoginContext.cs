using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace LoginApi.Model;

public class LoginContext : DbContext
{
    public LoginContext(DbContextOptions<LoginContext> options)
        : base(options)
    {
    }

    public DbSet<LoginItem> LoginItems { get; set; } = null!;
}